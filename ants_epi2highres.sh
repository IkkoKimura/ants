#! /bin/bash

# make required ROI transform from standard space(MNI 1mm) to diffusion space
# this assumes that you've already done ants normalization steps, and diffusion coregistration steps

base=$1
in=$2
out=$3
#ROI_name=$2 
#atlas=$3 # /home/ikko/atlas/vistasoft/MNI_JHU_tracts_ROIs 
#thr1=$4 # threshold for 1mm->2mm(e.g. 0.1)
#thr2=$5 # threshold for standard to highres, and highres to diffusion space(e.g. 0.1)
#ref=/home/ikimura/standard/MNI152_T1_2mm_brain.nii.gz

REG=$base/reg
#ROI=$base/roi

#1. 1mm-->2mm
#flirt -ref $ref -in $atlas/${ROI_name}.nii.gz -out $ROI/${ROI_name}_standard.nii.gz -applyisoxfm 2
#fslmaths $ROI/${ROI_name}_standard.nii.gz -thr $thr1 -bin $ROI/${ROI_name}_standard_bin.nii.gz
#2. epi to highres
antsApplyTransforms --dimensionality 3 --input $in --reference-image  ${REG}/highres_brain.nii.gz --output $out -n linear \
--transform ${REG}/epi2highres_ants0GenericAffine.mat 

