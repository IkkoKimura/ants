#! /bin/bash

# combine fsl transformation matrix with ANTs matrix (fmri,dmri...)
# ants_combine_fsl.sh $subj
# assuming the data is in the BIDS format https://bids.neuroimaging.io/
# Ikko Kimura, Osaka University, 2020/04/18


subj=$1
WD=${PWD}
standard=/home/ikko/work/antstest/standard
example_func=/home/ikko/work/antstest/sub-${subj}/func/sub-${subj}_task-rest_desc-preproc_bold_ref_brain.nii.gz
t1=$WD/sub-${subj}/T1_native.nii.gz
t1_brain=$WD/sub-${subj}/T1w_brain.nii.gz
nthreads=8


echo "-----------------------"
echo "Preparing for epi data"
echo "WD: $WD"
echo "Processing for subj-$subj"
echo "-----------------------"

cp $example_func ${WD}/sub-${subj}/example_func.nii.gz

if [ ! -e ${WD}/sub-${subj}/reg/epi2highres.mat ]; then
echo "!!! No epi2highres.mat !!!"
echo "creating files..."
epi_reg --epi=${example_func} --t1=$t1 --t1brain=$t1_brain --out=${WD}/sub-${subj}/reg/epi2highres
echo "Done!"
fi

# make fsl transformation file compatible to ANTs 
echo "converting fsl transformation files..."
c3d_affine_tool -src ${example_func} -ref ${t1_brain} ${WD}/sub-${subj}/reg/epi2highres.mat -fsl2ras -oitk ${WD}/sub-${subj}/reg/epi2highres.txt
sed -i -e '3s/MatrixOffsetTransformBase_double_3_3/AffineTransform_double_3_3/' ${WD}/sub-${subj}/reg/epi2highres.txt # change header info	

# Just for QC...
echo "example_func2highres"
antsApplyTransforms --dimensionality 3 --input ${example_func} --reference-image $t1_brain --output ${WD}/sub-${subj}/reg/example_func_highres.nii.gz -n linear \
--transform ${WD}/sub-${subj}/reg/epi2highres.txt
echo "highres2example_func"
antsApplyTransforms --dimensionality 3 --input $t1_brain --reference-image ${example_func} --output ${WD}/sub-${subj}/reg/highres_example_func.nii.gz -n linear \
--transform [${WD}/sub-${subj}/reg/epi2highres.txt,1]
echo "example_func2standard..."
antsApplyTransforms --dimensionality 3 --input ${example_func} --reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output ${WD}/sub-${subj}/reg/example_func_standard.nii.gz -n linear \
--transform ${WD}/sub-${subj}/reg/highres2standard1Warp.nii.gz --transform ${WD}/sub-${subj}/reg/highres2standard0GenericAffine.mat --transform ${WD}/sub-${subj}/reg/epi2highres.txt
echo "standard2example_func..."
antsApplyTransforms --dimensionality 3 --input $standard/MNI152_T1_2mm_brain.nii.gz --reference-image  ${example_func} --output ${WD}/sub-${subj}/reg/standard_example_func.nii.gz -n linear \
--transform [${WD}/sub-${subj}/reg/epi2highres.txt,1] --transform [${WD}/sub-${subj}/reg/highres2standard0GenericAffine.mat,1] --transform ${WD}/sub-${subj}/reg/highres2standard1InverseWarp.nii.gz

echo "Done!"
