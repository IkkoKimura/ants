% do SBCA(seed-baed correlation analysis with ANTs output)
% ants_SBCA.sh $subj
% assuming the data is in the BIDS format https://bids.neuroimaging.io/
% Ikko Kimura, Osaka University, 2020/04/18

function ants_scanner_2_voxel_coord(input_file,example_func,output_dir)
%input_file='/home/ikko/work/antstest/sub-01/roi/roilist.csv';
%example_func='/home/ikko/work/antstest/sub-01/func/sub-01_task-rest_desc-preproc_bold_ref_brain.nii.gz';
%output_dir='/home/ikko/work/antstest/sub-01/roi';

info = niftiinfo(example_func);

data = readmatrix(input_file);

for i=1:size(data,1)
voxel=ceil([data(i,1:3) 1]*(info.Transform.T)^(-1));
% voxel=voxel-1; unneeded?
writematrix(voxel(1:3)',fullfile(output_dir,['roi_',num2str(i),'.txt']),'Delimiter',' ')
end


