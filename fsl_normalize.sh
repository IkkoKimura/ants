#! /bin/bash

# perform normalization with FLIRT+FNIRT 
# ants_normalize.sh $subj
# assuming the data is in the BIDS format https://bids.neuroimaging.io/
# Ikko Kimura, Osaka University, 2020/06/03
#### UNDER CONSTRUCTION....
subj=$1
WD=$2 # setting the working directory is more flexible
standard=/home/ikimura/standard
# nthreads=8 # unfortunately, no 

echo "-----------------------"
echo "Normalization"
echo "WD: $WD"
echo "Processing for subj-$subj"
echo "-----------------------"

REG=${WD}/sub-$subj/reg
if [ ! -d $REG]; then
echo "making directory at $REG" 
mkdir $REG
fi

cp ${standard}/MNI152_T1_2mm.nii.gz ${WD}/sub-$subj/reg/standard.nii.gz
cp ${standard}/MNI152_T1_2mm_brain.nii.gz ${WD}/sub-$subj/reg/standard_brain.nii.gz
cp ${WD}/sub-$subj/T1w.nii.gz ${REG}/highres.nii.gz
cp ${WD}/sub-$subj/T1w_brain.nii.gz ${REG}/highres_brain.nii.gz


antsRegistrationSyN.sh -d 3 -f ${standard}/MNI152_T1_2mm_brain.nii.gz -m ${WD}/sub-$subj/T1w_brain.nii.gz -o ${WD}/sub-$subj/reg/highres2standard -n ${nthreads} -x ${standard}/MNI152_T1_2mm_brain_mask_dil1.nii.gz
