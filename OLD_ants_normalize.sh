#! /bin/bash

# perform normalization with ANTs 
# ants_normalize.sh $subj
# assuming the data is in the BIDS format https://bids.neuroimaging.io/
# Ikko Kimura, Osaka University, 2020/04/18

subj=$1
WD=${PWD}
standard=/home/ikimura/standard
nthreads=8

echo "-----------------------"
echo "2. Normalization"
echo "WD: $WD"
echo "Processing for $subj"
echo "-----------------------"
mkdir ${WD}/sub-$subj/reg
cp ${standard}/MNI152_T1_2mm_brain.nii.gz ${WD}/sub-$subj/reg/standard_brain.nii.gz
antsRegistrationSyN.sh -d 3 -f ${standard}/MNI152_T1_2mm_brain.nii.gz -m ${WD}/sub-$subj/T1w_brain.nii.gz -o ${WD}/sub-$subj/reg/highres2standard -n ${nthreads} -x ${standard}/MNI152_T1_2mm_brain_mask_dil1.nii.gz
