#! /bin/bash

# perform normalization with ANTs 
# ants_normalize.sh $subj $base $Nthreads
# this script is made for preprocessing pipeline in Vida dMRI
# Ikko Kimura, Osaka University, 2020/04/18
# Ikko Kimura, Osaka University, 2020/06/23(compatible with dMRI pipelines)

base=$1 # $base directory in Vida preprocessing pipeline
NThreads=$2
standard=/home/ikimura/standard

echo "Normalizing the data for $base"
#echo "Working Directory is $WD"

if [ ! -e $base/reg ]; then
echo "weired... but making directory at $base"
mkdir $base/reg
fi 

if [ ! -e ${base}/reg/highres_brain.nii.gz ]; then
echo "!!! YOU NEED ${base}/reg/highres_brain.nii.gz BEFOREHAND !!!"
fi 

cp ${standard}/MNI152_T1_2mm_brain.nii.gz ${base}/reg/standard_brain.nii.gz
antsRegistrationSyN.sh -d 3 -f ${base}/reg/standard_brain.nii.gz -m ${base}/reg/highres_brain.nii.gz -o ${base}/reg/highres2standard -n ${NThreads} -x ${standard}/MNI152_T1_2mm_brain_mask_dil1.nii.gz
