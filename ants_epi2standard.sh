#! /bin/bash

# diffusion space to standard space
# this assumes that you've already done ants normalization steps, and diffusion coregistration steps

base=$1
in=$2
out=$3

REG=$base/reg
#ROI=$base/roi

#
antsApplyTransforms --dimensionality 3 --input $in --reference-image  ${REG}/standard_brain.nii.gz --output $out -n linear \
--transform ${REG}/highres2standard1Warp.nii.gz --transform ${REG}/highres2standard0GenericAffine.mat --transform ${REG}/epi2highres_ants0GenericAffine.mat 

