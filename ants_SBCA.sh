#! /bin/bash

# do SBCA(seed-baed correlation analysis with ANTs output)
# ants_SBCA.sh $subj
# assuming the data is in the BIDS format https://bids.neuroimaging.io/
# Ikko Kimura, Osaka University, 2020/04/18


subj=$1
WD=${PWD}
standard=/home/ikko/work/antstest/standard
example_func=/home/ikko/work/antstest/sub-${subj}/func/sub-${subj}_task-rest_desc-preproc_bold_ref_brain.nii.gz
func=/home/ikko/work/antstest/sub-${subj}/func/sub-${subj}_task-rest_desc-preproc_bold.nii.gz
t1=$WD/sub-${subj}/T1_native.nii.gz
t1_brain=$WD/sub-${subj}/T1w_brain.nii.gz
# Do specifiy in the MNI coordinates
target=/home/ikko/work/antstest/ROI_MNI.csv
flag=0 # as I have two candidates for processing steps...
nthreads=8
num_roi=2

echo "-----------------------"
echo "Doing SBCA..."
echo "WD: $WD"
echo "Processing for subj-$subj"
echo "-----------------------"

if [ ! -d ${WD}/sub-${subj}/roi ]; then
	mkdir ${WD}/sub-${subj}/roi
fi
if [ ! -d ${WD}/sub-${subj}/time ]; then
	mkdir ${WD}/sub-${subj}/time
fi
# MNI point to example_func
antsApplyTransformsToPoints -d 3 -i $target -o ${WD}/sub-${subj}/roi/roilist.csv --transform [${WD}/sub-${subj}/reg/epi2highres.txt,1] --transform [${WD}/sub-${subj}/reg/highres2standard0GenericAffine.mat,1] --transform ${WD}/sub-${subj}/reg/highres2standard1InverseWarp.nii.gz

# scanner coordinates to voxel coordinates ## NEEDS REVISION
matlab -nodisplay -nodesktop -r "ants_scanner_2_voxel_coord('"${WD}/sub-${subj}/roi/roilist.csv"','"$example_func"','"${WD}/sub-${subj}/roi"'); exit"

for i in `seq 1 $num_roi`; do
	split -l 1 ${WD}/sub-${subj}/roi/roi_${i}.txt ${WD}/sub-${subj}/roi/roi_${i}
	fslmaths $example_func -mul 0 -add 1 -roi `cat ${WD}/sub-${subj}/roi/roi_${i}aa` 1 `cat ${WD}/sub-${subj}/roi/roi_${i}ab` 1 `cat ${WD}/sub-${subj}/roi/roi_${i}ac` 1 0 1 ${WD}/sub-${subj}/roi/roi_${i}_point
	fslmaths ${WD}/sub-${subj}/roi/roi_${i}_point -kernel sphere 4 -fmean ${WD}/sub-${subj}/roi/roi_${i}_sphere
	fslmaths ${WD}/sub-${subj}/roi/roi_${i}_sphere -bin ${WD}/sub-${subj}/roi/roi_${i}_sphere_bin
	# extract time series data
	#fslmeants -i ${func} -m ${WD}/sub-${subj}/roi/roi_${i}_sphere_bin -o ${WD}/sub-${subj}/time/roi_${i}.txt
	# data	
	dual_regression ${WD}/sub-${subj}/roi/roi_${i}_sphere_bin 0 -1 0 ${WD}/sub-${subj}/roi/roi_${i} $func
	cp ${WD}/sub-${subj}/roi/roi_${i}/dr_stage2_ic0000.nii.gz ${WD}/sub-${subj}/roi_${i}_sbca_beta.nii.gz
	rm ${WD}/sub-${subj}/roi/roi_${i}aa ${WD}/sub-${subj}/roi/roi_${i}ab ${WD}/sub-${subj}/roi/roi_${i}ac
done
