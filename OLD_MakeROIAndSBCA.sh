#! /bin/bash

standard=/home/ikko/work/ANTsComp/MNI # directory of standard 
dataset=/home/ikko/work/ANTsComp/MotionfMRI
#ANTS=/home/ikko/work/ANTsComp/${subj}_fs/ants
#FSL=/home/ikko/work/ANTsComp/${subj}_fs/fsl

ROI=lhMT
### THIS SHOULD BE DONE BEFOREHAND !
if [ ! -f ${ROI}sphere.nii.gz ]; then
echo "No ROI file...creating ROI sphere..."
fslmaths $standard/MNI152_T1_2mm_brain.nii.gz -mul 0 -add 1 -roi 46 1 29 1 54 1 0 1 ${ROI}point -odt float
fslmaths ${ROI}point -kernel sphere 4 -fmean ${ROI}sphere -odt float
fslmaths ${ROI}sphere.nii.gz -bin ${ROI}sphere_bin.nii.gz
echo "PLEASE CHECK THE ROI!!!" 
fi

#for subj in `cat subject.txt`; do
subj=$1	
	echo "Processing for $subj"
	wdir=/home/ikko/work/ccPAS2/$subj*
	ANTS=/home/ikko/work/ANTsComp/${subj}_fs/ants
	FSL=/home/ikko/work/ANTsComp/${subj}_fs/fsl
	if [ ! -f /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func12FS.mat ]; then
	echo "no example_func2FS files..."	
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func12FS.mat -concat /home/ikko/work/ANTsComp/${subj}_fs/fsl/Native2FS.mat $wdir/func1_Motion.feat/reg/example_func2highres.mat
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func22FS.mat -concat /home/ikko/work/ANTsComp/${subj}_fs/fsl/Native2FS.mat $wdir/func2_Motion.feat/reg/example_func2highres.mat
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func32FS.mat -concat /home/ikko/work/ANTsComp/${subj}_fs/fsl/Native2FS.mat $wdir/func3_Motion.feat/reg/example_func2highres.mat
	fi	
	#ANTS
	echo "ANTS..."
	SECONDS=0
	if [ ! -f $ANTS/example_func1.nii.gz ]; then
	echo "no examle func so fetching data..."
	echo "converting mat files..."
	cp ${wdir}/func1_Motion.feat/example_func.nii.gz $ANTS/example_func1.nii.gz
	cp ${wdir}/func2_Motion.feat/example_func.nii.gz $ANTS/example_func2.nii.gz
	cp ${wdir}/func3_Motion.feat/example_func.nii.gz $ANTS/example_func3.nii.gz
	fi
	if [ ! -f  ${ANTS}/example_func12FS.txt ]; then
	c3d_affine_tool -src $ANTS/example_func1.nii.gz -ref /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func12FS.mat -fsl2ras -oitk ${ANTS}/example_func12FS.txt
	c3d_affine_tool -src $ANTS/example_func2.nii.gz -ref /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func22FS.mat -fsl2ras -oitk ${ANTS}/example_func22FS.txt 
	c3d_affine_tool -src $ANTS/example_func3.nii.gz -ref /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func32FS.mat -fsl2ras -oitk ${ANTS}/example_func32FS.txt
	sed -i -e '3s/MatrixOffsetTransformBase_double_3_3/AffineTransform_double_3_3/' ${ANTS}/example_func12FS.txt
	sed -i -e '3s/MatrixOffsetTransformBase_double_3_3/AffineTransform_double_3_3/' ${ANTS}/example_func22FS.txt
	sed -i -e '3s/MatrixOffsetTransformBase_double_3_3/AffineTransform_double_3_3/' ${ANTS}/example_func32FS.txt
	fi	
	echo "inverse transforms..."
	antsApplyTransforms --dimensionality 3 --input ${ROI}sphere.nii.gz \
--reference-image  ${ANTS}/example_func1.nii.gz --output  ${ANTS}/${ROI}sphere_func1.nii.gz -n linear \
--transform [${ANTS}/example_func12FS.txt,1] --transform [$ANTS/highres2standard0GenericAffine.mat,1] --transform ${ANTS}/highres2standard1InverseWarp.nii.gz
	antsApplyTransforms --dimensionality 3 --input ${ROI}sphere.nii.gz \
--reference-image  ${ANTS}/example_func1.nii.gz --output  ${ANTS}/${ROI}sphere_func2.nii.gz -n linear \
--transform [${ANTS}/example_func22FS.txt,1] --transform [$ANTS/highres2standard0GenericAffine.mat,1] --transform ${ANTS}/highres2standard1InverseWarp.nii.gz
	antsApplyTransforms --dimensionality 3 --input ${ROI}sphere.nii.gz \
--reference-image  ${ANTS}/example_func1.nii.gz --output  ${ANTS}/${ROI}sphere_func3.nii.gz -n linear \
--transform [${ANTS}/example_func32FS.txt,1] --transform [$ANTS/highres2standard0GenericAffine.mat,1] --transform ${ANTS}/highres2standard1InverseWarp.nii.gz
	fslmaths ${ANTS}/${ROI}sphere_func1.nii.gz -bin ${ANTS}/${ROI}sphere_func1_bin.nii.gz
	fslmaths ${ANTS}/${ROI}sphere_func2.nii.gz -bin ${ANTS}/${ROI}sphere_func2_bin.nii.gz
	fslmaths ${ANTS}/${ROI}sphere_func3.nii.gz -bin ${ANTS}/${ROI}sphere_func3_bin.nii.gz
	echo "SBCA..."
	dual_regression ${ANTS}/${ROI}sphere_func1_bin.nii.gz 0 -1 0 ${ANTS}/${ROI}_func1.DR /home/ikko/work/ccPAS2/${subj}*/func1_Motion.feat/filtered_func_data.nii.gz 
	dual_regression ${ANTS}/${ROI}sphere_func2_bin.nii.gz 0 -1 0 ${ANTS}/${ROI}_func2.DR /home/ikko/work/ccPAS2/${subj}*/func2_Motion.feat/filtered_func_data.nii.gz
	dual_regression ${ANTS}/${ROI}sphere_func3_bin.nii.gz 0 -1 0 ${ANTS}/${ROI}_func3.DR /home/ikko/work/ccPAS2/${subj}*/func3_Motion.feat/filtered_func_data.nii.gz	
	echo "Transforming the results..."	
	antsApplyTransforms --dimensionality 3 --input ${ANTS}/${ROI}_func1.DR/dr_stage2_subject00000.nii.gz \
--reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output $ANTS/sbca_${ROI}_func1_standard_ants.nii.gz -n linear \
--transform $ANTS/highres2standard1Warp.nii.gz --transform $ANTS/highres2standard0GenericAffine.mat --transform ${ANTS}/example_func12FS.txt
	antsApplyTransforms --dimensionality 3 --input ${ANTS}/${ROI}_func2.DR/dr_stage2_subject00000.nii.gz \
--reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output $ANTS/sbca_${ROI}_func2_standard_ants.nii.gz -n linear \
--transform $ANTS/highres2standard1Warp.nii.gz --transform $ANTS/highres2standard0GenericAffine.mat --transform ${ANTS}/example_func22FS.txt
	antsApplyTransforms --dimensionality 3 --input ${ANTS}/${ROI}_func3.DR/dr_stage2_subject00000.nii.gz \
--reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output $ANTS/sbca_${ROI}_func3_standard_ants.nii.gz -n linear \
--transform $ANTS/highres2standard1Warp.nii.gz --transform $ANTS/highres2standard0GenericAffine.mat --transform ${ANTS}/example_func32FS.txt
	fslmerge -t $ANTS/sbca_${ROI}_func_all_standard_ants.nii.gz $ANTS/sbca_${ROI}_func*_standard_ants.nii.gz
	fslmaths $ANTS/sbca_${ROI}_func_all_standard_ants.nii.gz -Tmean $ANTS/sbca_${ROI}_func_mean_standard_ants.nii.gz
	time1=$SECONDS
	echo "Elapsed time was ${time1}seconds"	
	#FSL
	echo "FSL..."
	SECONDS=0
	echo "inverse transforms..."
	if [ ! -f  /home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func1.mat ]; then
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func1.mat -inverse /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func12FS.mat
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func2.mat -inverse /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func22FS.mat
	convert_xfm -omat /home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func3.mat -inverse /home/ikko/work/ANTsComp/${subj}_fs/fsl/example_func32FS.mat
	invwarp -w $FSL/highres2standard_warp.nii.gz -o $FSL/standard2highres_warp.nii.gz -r /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz
	fi	
	applywarp --ref=${ANTS}/example_func1.nii.gz --in=${ROI}sphere.nii.gz --out=${FSL}/${ROI}sphere_func1.nii.gz --warp=$FSL/standard2highres_warp.nii.gz --postmat=/home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func1.mat
	applywarp --ref=${ANTS}/example_func2.nii.gz --in=${ROI}sphere.nii.gz --out=${FSL}/${ROI}sphere_func2.nii.gz --warp=$FSL/standard2highres_warp.nii.gz --postmat=/home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func3.mat
	applywarp --ref=${ANTS}/example_func3.nii.gz --in=${ROI}sphere.nii.gz --out=${FSL}/${ROI}sphere_func3.nii.gz --warp=$FSL/standard2highres_warp.nii.gz --postmat=/home/ikko/work/ANTsComp/${subj}_fs/fsl/FS2example_func3.mat
	fslmaths ${FSL}/${ROI}sphere_func1.nii.gz -bin ${FSL}/${ROI}sphere_func1_bin.nii.gz
	fslmaths ${FSL}/${ROI}sphere_func2.nii.gz -bin ${FSL}/${ROI}sphere_func2_bin.nii.gz
	fslmaths ${FSL}/${ROI}sphere_func3.nii.gz -bin ${FSL}/${ROI}sphere_func3_bin.nii.gz
	echo "SBCA..."
	dual_regression ${FSL}/${ROI}sphere_func1_bin.nii.gz 0 -1 0 ${FSL}/${ROI}_func1.DR /home/ikko/work/ccPAS2/${subj}*/func1_Motion.feat/filtered_func_data.nii.gz 
	dual_regression ${FSL}/${ROI}sphere_func2_bin.nii.gz 0 -1 0 ${FSL}/${ROI}_func2.DR /home/ikko/work/ccPAS2/${subj}*/func2_Motion.feat/filtered_func_data.nii.gz
	dual_regression ${FSL}/${ROI}sphere_func3_bin.nii.gz 0 -1 0 ${FSL}/${ROI}_func3.DR /home/ikko/work/ccPAS2/${subj}*/func3_Motion.feat/filtered_func_data.nii.gz	
	echo "Transforming the results..."
	applywarp --ref=$standard/MNI152_T1_2mm_brain.nii.gz --in=${FSL}/${ROI}_func1.DR/dr_stage2_subject00000.nii.gz --out=$FSL/sbca_${ROI}_func1_standard_fsl.nii.gz --warp=$FSL/highres2standard_warp.nii.gz --premat=${FSL}/example_func22FS.mat
	applywarp --ref=$standard/MNI152_T1_2mm_brain.nii.gz --in=${FSL}/${ROI}_func2.DR/dr_stage2_subject00000.nii.gz --out=$FSL/sbca_${ROI}_func2_standard_fsl.nii.gz --warp=$FSL/highres2standard_warp.nii.gz --premat=${FSL}/example_func22FS.mat
	applywarp --ref=$standard/MNI152_T1_2mm_brain.nii.gz --in=${FSL}/${ROI}_func3.DR/dr_stage2_subject00000.nii.gz --out=$FSL/sbca_${ROI}_func3_standard_fsl.nii.gz --warp=$FSL/highres2standard_warp.nii.gz --premat=${FSL}/example_func22FS.mat
	fslmerge -t $FSL/sbca_${ROI}_func_all_standard_fsl.nii.gz $FSL/sbca_${ROI}_func*_standard_fsl.nii.gz
	fslmaths $FSL/sbca_${ROI}_func_all_standard_fsl.nii.gz -Tmean $FSL/sbca_${ROI}_func_mean_standard_fsl.nii.gz
	time2=$SECONDS
	echo "Elapsed time was ${time2}seconds"
	echo "Done!"
#done

echo "-----------------------"
echo "ANTs: ${time1}sec"
echo "FSL: ${time2}sec"
echo "-----------------------"
