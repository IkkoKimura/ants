#! /bin/bash

# perform brain extraction 
# ants_bet.sh $subj
# assuming the data is in the BIDS format https://bids.neuroimaging.io/
# Ikko Kimura, Osaka University, 2020/04/18

subj=$1
WD=${PWD}
flag1=0 #1:perform N4BiasFieldCorrection
flag2=2 #0:fsl,1:freesurfer,2:ants
temp=/home/ikko/work/antstest/standard/MICCAI2012-Multi-Atlas-Challenge-Data # specifiy template folders
fsdir=/home/ikko/work/fmriprep_test/output/fmriprep/freesurfer # fs output from recon-all

echo "-----------------------"
echo "1. Brain Extraction"
echo "WD: $WD"
echo "Processing for subj-$subj"
echo "-----------------------"

mkdir sub-$subj

# N4 bias correction
if [ $flag1 -eq 1 ]; then
echo "performing N4BiasFieldCorrection"
	N4BiasFieldCorrection -d 3 -i ${WD}/data/sub-${subj}/anat/sub-${subj}_T1w.nii -s 2 -o ${WD}/sub-$subj/T1w.nii
else 
	cp ${WD}/data/sub-${subj}/anat/sub-${subj}_T1w.nii ${WD}/sub-$subj/T1w.nii
fi

# Brain Extraction
if [ $flag2 -eq 0 ]; then
	echo "Using fsl bet program as brain extracted data..."
	bet2  ${WD}/sub-$subj/T1w ${WD}/sub-$subj/T1w_brain
elif [ $flag2 -eq 1 ]; then
	echo "Using freesurfer file as brain extracted data..."
	# freesurfer files must be specified beforehand...will revise later
	fs=$fsdir/sub-${subj}/mri
	mri_vol2vol --mov $fs/T1.mgz --targ $fs/rawavg.mgz --regheader --o ${WD}/sub-$subj/T1_native.mgz --no-save-reg # nucorrected intensity normalized brain(N3)
	mri_vol2vol --mov $fs/brain.mgz --targ $fs/rawavg.mgz --regheader --o ${WD}/sub-$subj/brain_native.mgz --no-save-reg # # nucorrected intensity normalized skull stripped brain
	mri_convert ${WD}/sub-$subj/T1_native.mgz ${WD}/sub-$subj/T1_native.nii.gz
	mri_convert ${WD}/sub-$subj/brain_native.mgz ${WD}/sub-$subj/brain_native.nii.gz
	cp ${WD}/sub-$subj/brain_native.nii.gz ${subj}_fs/T1w_brain.nii.gz
	#fslreorient2std ${subj}_fs/T1w.nii.gz  ${subj}_fs/T1w_brain.nii.gz
	#fslreorient2std ${subj}_fs/T1w_brain.nii.gz  ${subj}_fs/T1w_RPI_brain.nii.gz
	#
else
	echo "Using ANTs for brain extraction..." 
	# Unfortunately we must first set the orientation
	# e.g.fslswapdim sub-03_T1w_bk.nii LR SI PA sub-03_T1w.nii --> not a matter 
	antsBrainExtraction.sh -d 3 -a ${WD}/sub-$subj/T1w.nii -e $temp/T_template0.nii.gz \
	-m $temp/T_template0_BrainCerebellumProbabilityMask.nii.gz -o ${WD}/sub-$subj/T1w_ -k 1 # N4 is corrected in this script
	# change the name for compatibility
	mv ${WD}/sub-$subj/T1w_BrainExtractionBrain.nii.gz ${WD}/sub-$subj/T1w_brain.nii.gz
fi

echo "Done!"

