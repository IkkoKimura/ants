#! /bin/bash

# process flirt+fnirt and antsRegistration for comparison
# This time, I used freesurfer output as brain extracted data
# Ikko Kimura, Osaka University Medical Hospital, 2019/01/25 
standard=/home/ikko/work/ANTsComp/MNI # directory of standard

#subj=$1
#flag=0
#subj=OHY007

for subj in `cat subject.txt`; do
#
standard=/home/ikko/work/ANTsComp/MNI # directory of standard 
#temp=/home/ikko/work/ANTsComp/OASIS # template for ants brain extraction
fs=${SUBJECTS_DIR}/${subj}/mri
ANTS=/home/ikko/work/ANTsComp/${subj}_fs/ants
FSL=/home/ikko/work/ANTsComp/${subj}_fs/fsl

#fast -o ${subj}_fs/fast  ${subj}_fs/T1w_RPI_brain.nii.gz 
mri_convert $fs/filled.mgz ${subj}_fs/filled.nii.gz
fslmaths ${subj}_fs/filled.nii.gz -bin ${subj}_fs/wm.nii.gz
fslreorient2std ${subj}_fs/wm.nii.gz ${subj}_fs/wm_RPI.nii.gz
#ANTs
antsApplyTransforms --dimensionality 3 --input ${subj}_fs/wm_RPI.nii.gz \
--reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output $ANTS/wm_RPI_standard.nii.gz -n linear \
--transform $ANTS/highres2standard1Warp.nii.gz --transform $ANTS/highres2standard0GenericAffine.mat
#FSL
applywarp --ref=$standard/MNI152_T1_2mm_brain.nii.gz --in=${subj}_fs/wm_RPI.nii.gz --out=$FSL/wm_RPI_standard.nii.gz --warp=$FSL/highres2standard_warp.nii.gz 
done
