# README 
This is just the demo for the normalizarion with ANTs

## DEMO
subj=01

ants_bet.sh $subj # this requires freeserfer outputs

ants_normalize.sh $subj

#### if you have epi data...
ants_combine_fsl.sh $subj
#### if you have fmri data...
ants_SBCA.sh $subj

## TO DO
また時間があれば、もっと一般的に使えるようにします...