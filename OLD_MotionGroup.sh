#! /bin/bash
# do normalization through ANTS and FSL, then do group analysis
# Ikko Kimura, Osaka University Medical Hospital, 2020/01/29

standard=/home/ikko/work/ANTsComp/MNI # directory of standard 


#1. Normalization step
for subj in `cat subject.txt`; do
echo "Processing for $subj"

ANTS=/home/ikko/work/ANTsComp/${subj}_fs/ants
FSL=/home/ikko/work/ANTsComp/${subj}_fs/fsl
#applywarp -i /home/ikko/work/ANTsComp/MotionFiles/Motion_${subj}.nii.gz -o $FSL/Motion_standard.nii.gz -r $standard/MNI152_T1_2mm_brain.nii.gz -w $FSL/highres2standard_warp.nii.gz
bet data/${subj}.nii.gz data/${subj}_brain.nii.gz
flirt -in data/${subj}_brain.nii.gz -ref /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz -omat $FSL/Native2FS.mat
flirt -in /home/ikko/work/ANTsComp/MotionFiles/Motion_${subj}.nii.gz -ref /home/ikko/work/ANTsComp/${subj}_fs/T1w_RPI_brain.nii.gz -applyxfm -init $FSL/Native2FS.mat -out /home/ikko/work/ANTsComp/MotionFiles/Motion_${subj}_fs.nii.gz

applywarp --ref=$standard/MNI152_T1_2mm_brain.nii.gz --in=/home/ikko/work/ANTsComp/MotionFiles/Motion_${subj}.nii.gz --out=$FSL/Motion_standard.nii.gz --warp=$FSL/highres2standard_warp.nii.gz --premat=$FSL/Native2FS.mat
antsApplyTransforms --dimensionality 3 --input /home/ikko/work/ANTsComp/MotionFiles/Motion_${subj}_fs.nii.gz \
--reference-image $standard/MNI152_T1_2mm_brain.nii.gz --output $ANTS/Motion_standard.nii.gz -n linear \
--transform $ANTS/highres2standard1Warp.nii.gz --transform $ANTS/highres2standard0GenericAffine.mat

echo "Done!"
done

#2. Group Analysis
fslmerge -t Motion_standard_ants_all.nii.gz OHY0*_fs/ants/Motion_standard.nii.gz
fslmerge -t Motion_standard_fsl_all.nii.gz OHY0*_fs/fsl/Motion_standard.nii.gz
randomise -i Motion_standard_ants_all.nii.gz -o Motion_ants -1 -v 5 -T -R
randomise -i Motion_standard_fsl_all.nii.gz -o Motion_fsl -1 -v 5 -T -R

#3. Project each result to fsaverage
mri_vol2surf --src Motion_ants_tfce_corrp_tstat1.nii.gz --out lh.Motion_ants_tfce_corrp_surf.mgh --hemi lh --mni152reg --projfrac 0.5
#mri_vol2surf --src Motion_ants_tfce_corrp_tstat1.nii.gz --out rh.Motion_ants_tfce_corrp_surf.mgh --hemi rh --mni152reg
mri_vol2surf --src Motion_fsl_tfce_corrp_tstat1.nii.gz --out lh.Motion_fsl_tfce_corrp_surf.mgh --hemi lh --mni152reg --projfrac 0.5
#mri_vol2surf --src Motion_fsl_tfce_corrp_tstat1.nii.gz --out rh.Motion_fsl_tfce_corrp_surf.mgh --hemi rh --mni152reg
mri_vol2surf --src Motion_ants_tstat1.nii.gz --out lh.Motion_ants_tstat1_surf.mgh --hemi lh --mni152reg --projfrac 0.5
#mri_vol2surf --src Motion_ants_tfce_corrp_tstat1.nii.gz --out rh.Motion_ants_tfce_corrp_surf.mgh --hemi rh --mni152reg
mri_vol2surf --src Motion_fsl_tstat1.nii.gz --out lh.Motion_fsl_tstat1_surf.mgh --hemi lh --mni152reg --projfrac 0.5
#mri_vol2surf --src Motion_fsl_tfce_corrp_tstat1.nii.gz --out rh.Motion_fsl_tfce_corrp_surf.mgh --hemi rh --mni152reg

